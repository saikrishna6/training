package com.virtusa.kabanapp.List.client;

import com.virtusa.kabanapp.List.model.CardItem;
import com.virtusa.kabanapp.List.model.ListItemDB;
import com.virtusa.kabanapp.List.model.ListToCards;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import java.util.List;
import java.util.stream.Collectors;

public class CardClient {

    final RestTemplate restTemplate = new RestTemplate();
    public Observable<ListToCards> listToCardsMapping(List<ListToCards> listOfListToCards, String cardsResourceURI){

        Observable<ListToCards> listToCardsObservable =  Observable.create(

                emmiter -> {
                    listOfListToCards.stream().map(lisToCards -> {
                        ResponseEntity<CardItem> responseEntity =  restTemplate.getForEntity(cardsResourceURI + String.valueOf(lisToCards.getId()), CardItem.class);
                        lisToCards.setCards(responseEntity.getBody().getCardList());
                        return lisToCards;
                    });
                }
        );
        return listToCardsObservable;
    }

    public List<ListToCards> syncRestTemplateCall(String cardsResourceURI,List<ListToCards> listOfListToCards){

        return listOfListToCards.stream().map(lisToCards -> {

            ResponseEntity<CardItem> responseEntity = restTemplate.getForEntity(cardsResourceURI + String.valueOf(lisToCards.getId()), CardItem.class);
            lisToCards.setCards(responseEntity.getBody().getCardList());
            return lisToCards;
        }).collect(Collectors.toList());
    }

    public Observable<List<ListToCards>> AsyncRestTemplateCall(List<ListToCards> listOfListToCards,String cardsResourceURI){

        return Observable.fromCallable(() -> syncRestTemplateCall(cardsResourceURI, listOfListToCards))
                .subscribeOn(Schedulers.io()).observeOn(Schedulers.computation())
                .observeOn(Schedulers.computation());

    }

}

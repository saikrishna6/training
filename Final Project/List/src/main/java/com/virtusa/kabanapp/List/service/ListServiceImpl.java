package com.virtusa.kabanapp.List.service;

import com.virtusa.kabanapp.List.client.CardClient;
import com.virtusa.kabanapp.List.model.*;
import com.virtusa.kabanapp.List.repository.ListRepository;
import io.reactivex.Observable;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.hibernate.exception.SQLGrammarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ListServiceImpl implements ListService {
    @Autowired
    private ListRepository listRepository;

    //AutoWire the restTemplate Assignment.
    // @Autowired
    // RestTemplate restTemplate;
    private Logger logger = LoggerFactory.getLogger(ListServiceImpl.class);

    public List<ListToCards> getLists(int boardId) {
        logger.debug("reached getLists() in ListService class");

        final RestTemplate restTemplate = new RestTemplate();

        // Replace this URI hard coding with configuration in application.properies
        String cardsResourceURI = "http://localhost:9003/cards/";
        List<ListItemDB> listItemDBLists = null;

        listItemDBLists = listRepository.findByBoardId(boardId);
        logger.info("lists retrieval successfull");

   /*     List<ListToCards> listOfListToCards  = listItemDBLists.stream().map(list -> {
                                                             ListToCards lisToCards = new ListToCards(list.getId(),list.getName(),list.getBoardId());
                                                             return lisToCards;
        }).collect(Collectors.toList());
*/
            List<ListToCards> listOfListToCards = listItemDBLists.stream().map(list -> {
            ListToCards lisToCards = new ListToCards(list.getListId(), list.getName(), list.getBoardId());
            return lisToCards;
        }).collect(Collectors.toList());

      Observable<ListToCards> listToCardsObservable = Observable.create(
              emmiter -> {

                  listOfListToCards.stream().forEach(lisToCards -> {
                            try {
                                ResponseEntity<CardItem> responseEntity = restTemplate.getForEntity(cardsResourceURI + String.valueOf(lisToCards.getId()), CardItem.class);
                                lisToCards.setCards(responseEntity.getBody().getCardList());
                                emmiter.onNext(lisToCards);;

                            }catch(Exception e){
                                emmiter.onError(e);
                            }
                            }
                  );
                  emmiter.onComplete();
              }
      );
        return listToCardsObservable.toList().blockingGet();
              //emmiter.onNext()
              //emmiter.complete()
              // rest Template in map and subscribe.
              // catch rest temp,ate exception. emmiter.error()

        //return listToCardsObservable.toList().blockingGet();

     //   Observable<ListToCards> listToCardsObservable1 = Observable.fromCallable(

   //      Synchronous call.

         return listOfListToCards.stream().map(lisToCards -> {

            ResponseEntity<CardItem> responseEntity = restTemplate.getForEntity(cardsResourceURI + String.valueOf(lisToCards.getId()), CardItem.class);
            lisToCards.setCards(responseEntity.getBody().getCardList());
            return lisToCards;
        }).collect(Collectors.toList());

         retun
      //cardClient.AsyncRestTemplateCall(listOfListToCards, cardsResourceURI) .toList().blockingGet();

    }

    public String addList(ListObject list) {

        logger.debug("reached addList() in ListService class");
        ListItemDB listItemDB = new ListItemDB(list.getName(), list.getBoardId());


        listRepository.save(listItemDB);
        listRepository.save(listItemDB);
        logger.info("List creation successfull");
        //remove hardCoding.
        return "List Creation Successful";
    }

    @Transactional
    public String deleteList(int listId) {
        //listRepository.deleteById(listId);
        logger.debug("reached deleteList() in ListController");
        final RestTemplate restTemplate = new RestTemplate();
        String cardsResourceURI = "http://localhost:9003/cards/";
        String cardsDeleteResourceURI = "http://localhost:9003/cards/deletecard/";

        ResponseEntity<CardItem> responseEntity = restTemplate.getForEntity(cardsResourceURI + String.valueOf(listId), CardItem.class);
        //lisToCards.setCards(responseEntity.getBody().getCardList());

        List<CardItemDB> cardList = responseEntity.getBody().getCardList();
        cardList.stream().forEach(card -> {

            restTemplate.delete(cardsDeleteResourceURI + String.valueOf(card.getCardId()));
        });

        logger.debug("listId to be deleted: " + String.valueOf(listId));
        listRepository.deleteByListId(listId);
        // listService.deleteList(listId);
        logger.info("ListID: " + String.valueOf(listId) + " deleted successfully.");
        return "Deleted Successfully";
    }

    public String updateList(int listId, String listName) {

        ListItemDB listItemDB = listRepository.findByListId(listId);
        listItemDB.setName(listName);
        listRepository.save(listItemDB);
        return "list updation successfull";
    }
}

package com.virtusa.kabanapp.List.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

@Table(name = "card")
public class CardItemDB {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cardId;
    private String name;
    //private int id;
    //@ManyToOne(targetEntity = ListItemDB.class, cascade = CascadeType.ALL)
    //private ListItemDB list;
    private int listId;
    //private int list_id;
    //private String title;
    private String description;
    private Date dueDate;
}

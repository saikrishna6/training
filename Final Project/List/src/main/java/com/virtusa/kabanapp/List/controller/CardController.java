package com.virtusa.kabanapp.List.controller;

import com.virtusa.kabanapp.List.model.Acknowledgement;
import com.virtusa.kabanapp.List.model.CardItem;
import com.virtusa.kabanapp.List.model.CardItemDB;
import com.virtusa.kabanapp.List.service.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cards/")
public class CardController {

    @Autowired
    CardService cardService;
    @Autowired
    Acknowledgement acknowledgement;

    @GetMapping("{listId}")
    public ResponseEntity<Object> getCards(@PathVariable("listId") int listId){

        List<CardItemDB> cards =  cardService.getCards(listId);
        CardItem cardItem = new CardItem(cards);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(cardItem);
    }

    @DeleteMapping("deletecard/{cardId}")
    public ResponseEntity<Acknowledgement> deleteCard(@PathVariable("cardId") int cardId){
        String statusMessage = cardService.deleteCard(cardId);
        acknowledgement.setMessage(statusMessage);
        return ResponseEntity.status(HttpStatus.MULTI_STATUS).body(acknowledgement);
    }
}

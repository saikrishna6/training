package com.virtusa.kabanapp.List.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class Acknowledgement {
    String message;
    int id;
    int statusCode;
}

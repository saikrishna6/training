package com.virtusa.kabanapp.List.controller;

import com.virtusa.kabanapp.List.model.*;
import com.virtusa.kabanapp.List.service.ListService;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Getter
@Setter
@RestController
@RequestMapping("/lists/")
public class ListController {
    @Autowired
    private ListService listService;
    @Autowired
    private ListItem listItem;
    @Autowired
    private Acknowledgement acknowledgement;
    private Logger logger = LoggerFactory.getLogger(ListController.class);

    @GetMapping("{boardid}")
    public ResponseEntity<ListItem> getLists(@PathVariable("boardid") int boardId) {
        logger.debug("reached GetLists() in ListController");
        listItem.setList(listService.getLists(boardId));
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(listItem);        //return listService.getLists(boardId);
    }
    //remove /
    @PostMapping("createlist")
    public ResponseEntity<Acknowledgement> addList(@RequestBody ListObject list) {
        logger.debug("reached addList() in ListController");
        String statusMessage = listService.addList(list);
        acknowledgement.setMessage(statusMessage);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(acknowledgement);
    }

    @DeleteMapping("{listId}")
    public ResponseEntity<Acknowledgement> deleteList(@PathVariable("listId") int listId) {
        String status = listService.deleteList(listId);
        acknowledgement.setMessage(status);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(acknowledgement);
    }

    @PatchMapping("{listId}/{listName}")
    public ResponseEntity<Acknowledgement> updateListName(@PathVariable("listId") int listId, @PathVariable("listName") String listName) {
        String status = listService.updateList(listId, listName);
        acknowledgement.setMessage(status);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(acknowledgement);
    }
}
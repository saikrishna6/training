package com.virtusa.kabanapp.List.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ListObject {
    String name;
    int boardId;
}

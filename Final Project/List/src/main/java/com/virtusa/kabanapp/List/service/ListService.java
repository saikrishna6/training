package com.virtusa.kabanapp.List.service;

import com.virtusa.kabanapp.List.model.ListObject;
import com.virtusa.kabanapp.List.model.ListToCards;
import java.util.List;

public interface ListService {

    List<ListToCards> getLists(int boardId);
    String addList(ListObject list);
    String deleteList(int listId);
    String updateList(int listId, String listName);

}

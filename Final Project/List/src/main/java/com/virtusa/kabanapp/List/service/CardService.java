package com.virtusa.kabanapp.List.service;

import com.virtusa.kabanapp.List.model.CardItemDB;
import com.virtusa.kabanapp.List.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CardService {

    @Autowired
    CardRepository cardRepository;

    public List<CardItemDB> getCards(int listId){
        return cardRepository.findByListId(listId);
    }
    @Transactional
    public String deleteCard(int cardId){
            cardRepository.deleteById(cardId);
            return "Operation Successful";
    }
}

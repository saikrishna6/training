package com.virtusa.kabanapp.List.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/*
* This is Model Object for Presentation to Represent List to Cards Mapping.
*
*/

@Setter
@Getter
public class ListToCards {

        private int id;
        private String name;
        private int boardId;
        private List<CardItemDB> cards;

        public ListToCards(int id, String name, int boardId){
            this.id = id;
            this.name = name;
            this.boardId = boardId;
        }
    }


package com.virtusa.kabanapp.List.repository;

import com.virtusa.kabanapp.List.model.CardItemDB;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface CardRepository extends JpaRepository<CardItemDB, Integer> {

    List<CardItemDB> findByListId(int id);
    //void deleteById(int id);
    void deleteByCardId(int cardId);
}

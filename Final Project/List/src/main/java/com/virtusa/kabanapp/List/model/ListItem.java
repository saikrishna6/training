package com.virtusa.kabanapp.List.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
@Getter
@Setter
//rename it as .//Dto
public class ListItem {
    List<ListToCards> list;
}

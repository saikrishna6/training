package com.virtusa.kabanapp.List.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor

@Table(name = "List")
public class ListItemDB {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int listId;
    //private int id;
    private String name;
    private int boardId;

   // @OneToMany(targetEntity = CardItemDB.class, cascade = CascadeType.ALL)
   // @JoinColumn(name="listId")
    //private List<CardItemDB> cards;

    public ListItemDB(String name, int boardId){
        this.name = name;
        this.boardId = boardId;
    }
}

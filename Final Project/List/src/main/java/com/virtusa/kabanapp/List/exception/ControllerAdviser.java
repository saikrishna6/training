package com.virtusa.kabanapp.List.exception;

import com.virtusa.kabanapp.List.model.Acknowledgement;
import com.virtusa.kabanapp.List.service.ListServiceImpl;
import org.hibernate.PessimisticLockException;
import org.hibernate.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ControllerAdviser {
    @Autowired
    Acknowledgement ackObj;
    Logger logger = LoggerFactory.getLogger(ListServiceImpl.class);

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Acknowledgement> handleListConstraintViolationException(){
        ackObj.setMessage("ContraintViolation Exception Occured");
        logger.error("ContraintViolation Exception Occured");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ackObj);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Acknowledgement> handleDataIntegrityViolationException(){
        ackObj.setMessage("DataIntegrityViolationException Occured");
        logger.error("DataIntegrityViolationException Occured");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ackObj);
    }

    @ExceptionHandler(org.hibernate.exception.DataException.class)
    public ResponseEntity<Acknowledgement> handleDataException(){
        ackObj.setMessage("DataException Occured");
        logger.error("DataException Occured");
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(ackObj);
    }

    @ExceptionHandler(SQLGrammarException.class)
    public ResponseEntity<Acknowledgement> handleSQLGrammarException(){
        ackObj.setMessage("SQLGrammar Exception Occured");
        logger.error("SQLGrammar Exception Occured");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ackObj);
    }

    @ExceptionHandler(org.hibernate.exception.GenericJDBCException.class)
    public ResponseEntity<Acknowledgement> handleGenericJDBCException(){
        ackObj.setMessage("GenericJDBCException Occured");
        logger.error("GenericJDBCException Occured");
        return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body(ackObj);
    }

    @ExceptionHandler(org.hibernate.QueryTimeoutException.class)
    public ResponseEntity<Acknowledgement> handleQueryTimeoutException(){
        ackObj.setMessage("QueryTimeoutException Occured");
        logger.error("QueryTimeoutException Occured");
        return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY).body(ackObj);
    }

    @ExceptionHandler(LockAcquisitionException.class)
    public ResponseEntity<Acknowledgement> handleLockAcquisitionException(){
        ackObj.setMessage("LockAcquisitionException Occured");
        logger.error("LockAcquisitionException Occured");
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(ackObj);
    }

    @ExceptionHandler(JDBCConnectionException.class)
    public ResponseEntity<Acknowledgement> handleLJDBCConnectionException(){
        ackObj.setMessage("JDBCConnectionException Occured");
        logger.error("JDBCConnectionException Occured");
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(ackObj);
    }

    @ExceptionHandler(PessimisticLockException.class)
    public ResponseEntity<Acknowledgement> handlePessimisticLockException(){
        ackObj.setMessage("PessimisticLockException occured");
        logger.error("PessimisticLockException occured");
        return ResponseEntity.status(HttpStatus.LOCKED).body(ackObj);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Acknowledgement> hanldeGenericException(){
        ackObj.setMessage("Exception Occured, Operation un-successfull");
        logger.error("Exception Occured, Operation un-successfull");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ackObj);
    }
}



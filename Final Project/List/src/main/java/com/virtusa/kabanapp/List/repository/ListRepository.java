package com.virtusa.kabanapp.List.repository;

import com.virtusa.kabanapp.List.model.ListItemDB;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.*;

@Repository
public interface ListRepository extends JpaRepository<ListItemDB, Integer> {

      List<ListItemDB> findByBoardId(int id);
      void deleteByListId(int listId);
      ListItemDB findByListId(int listId);
}


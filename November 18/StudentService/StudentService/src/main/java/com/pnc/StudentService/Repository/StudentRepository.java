package com.pnc.StudentService.Repository;

import java.util.*;
import com.pnc.StudentService.Model.*;

public interface StudentRepository {

    public List<Student> getStudents();
    public Student getstudentById(int id);

}

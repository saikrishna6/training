package com.pnc.StudentService.Controller;

import com.pnc.StudentService.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pnc.StudentService.Model.Student;
import com.pnc.StudentService.Service.StudenService;


import java.util.*;

@RestController
@RequestMapping("/students/")
public class StudentServiceController {

    @Autowired
    StudentRepository studentRepo;
    @Autowired
    StudenService studentService;


    @GetMapping("")
    public List<Student> getStudents(){

    //    return studentRepo.getStudents();
           return studentService.getStudents();
    }

    @GetMapping("{id}")
    public Student getStudentById(@PathVariable("id") int id){

        //return studentRepo.getstudentById(id);
         return studentService.getStudentById(id);
    }

    @GetMapping("firstNameStartWith/{startsWith}")
    public List<Student> getStudentByFirstNameStartWith(@PathVariable("startsWith") String str){

        return studentService.getStudentByFirstNameStartWith(str);
    }
}

package com.pnc.StudentService.Repository;

import org.springframework.stereotype.Repository;
import com.pnc.StudentService.Model.Student;
import java.util.*;

@Repository
public class StudentRepositoryImpl implements StudentRepository {

    List<Student> students;

   public StudentRepositoryImpl(){

       students = new ArrayList<>();
       students.add(new Student(1, "Sai", "Krishna"));
       students.add(new Student(2, "Krishna ", "Telukuntla"));
       students.add(new Student(3, "Bob", "Miller"));
    }

    public List<Student> getStudents(){

       return students;
    }

    public Student getstudentById(int id){

       //students.stream().filter(student -> student.studentId == id).

        int listLen = students.size();
        Student student = null;

        for(int i = 0 ; i  < listLen ; i++){

            if(students.get(i).getStudentId() ==  id){

                student = students.get(i);
                break;
            }
        }
        return student;
    }
}

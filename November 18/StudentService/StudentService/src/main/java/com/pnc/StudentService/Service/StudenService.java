package com.pnc.StudentService.Service;

import com.pnc.StudentService.Model.Student;

import java.util.List;

public interface StudenService {

    public List<Student> getStudents();
    public Student getStudentById(int id);
    public List<Student> getStudentByFirstNameStartWith(String a);
}

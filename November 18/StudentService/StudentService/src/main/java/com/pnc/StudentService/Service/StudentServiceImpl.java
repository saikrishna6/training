package com.pnc.StudentService.Service;

import com.pnc.StudentService.Model.Student;
import com.pnc.StudentService.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudenService {

    private static String startsWith = null;
    @Autowired
    StudentRepository repo;

    public List<Student> getStudents(){

        return repo.getStudents();
    }

    public Student getStudentById(int id){

        return repo.getstudentById(id);
    }

    public List<Student> getStudentByFirstNameStartWith(String a){

        StudentServiceImpl.startsWith = a;
        List<Student> list =  repo.getStudents();

       return list.stream().filter(student -> student.getFirstName().startsWith(StudentServiceImpl.startsWith)).collect(Collectors.toList());

    }

/*    public List<Student> getStudentByNameStartWith(String a){

        StudentServiceImpl.startsWith = a;
        List<Student> list =  repo.getStudents();

        List<Student> list1 = null;
        List<Student> list2 = null;
        list1 =  list.stream().filter(student -> student.getFirstName().startsWith(StudentServiceImpl.startsWith)).collect(Collectors.toList());
        //list2 =
    } */

}

package com.pnc.StudentService.Model;

import java.io.Serializable;

public class Student implements Serializable {

    int studentId;

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    String firstName;
    String lastName;

    public int getStudentId() {
        return studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Student(int id, String firstName, String lastName){

        this.studentId = id;
        this.firstName = firstName;
        this.lastName = lastName;

    }

}

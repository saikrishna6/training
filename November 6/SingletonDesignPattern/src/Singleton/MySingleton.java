package Singleton;

public enum MySingleton {
    INSTANCE;

    public String getDesignPattern(){

        return "Singleton";
    }
}


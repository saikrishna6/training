package Singleton;

public class Main {

    public static void main(String[] args){

        MySingleton singletonInstance = MySingleton.INSTANCE;
        System.out.println("Design Pattern Implemented: " + singletonInstance.getDesignPattern());
    }
}

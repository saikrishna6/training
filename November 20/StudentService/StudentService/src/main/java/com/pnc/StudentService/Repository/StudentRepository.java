package com.pnc.StudentService.Repository;

import java.util.*;
import com.pnc.StudentService.Model.*;
import org.springframework.http.ResponseEntity;

public interface StudentRepository {

    public List<Student> getStudents();
    public Student getstudentById(int id);
    public ResponseEntity<Object> addStudent(final Student student);
    public ResponseEntity<Object> addStudentProfile(final StudentProfile studentProfile);
    public ResponseEntity<Object> getStudentProfile(int id);
    public ResponseEntity<Object> updateStudentProfile(StudentProfile studentProfile);


    }

package com.pnc.StudentService.Service;

import com.pnc.StudentService.Model.Student;
import com.pnc.StudentService.Model.StudentEntity;
import com.pnc.StudentService.Model.StudentProfile;
import com.pnc.StudentService.Model.StudentProfileEntity;
import com.pnc.StudentService.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudenService {

    private static String startsWith = null;

    @Autowired
    StudentDtoRepository studentDtoRepository;

    @Autowired
    StudentProfileDtoRepository studentProfileDtoRepository;

    @Autowired
    StudentRepository repo ;

    @Override
    public Student getStudentById(int id){

        StudentEntity studentEntity = studentDtoRepository.findById(id);
        Student student = new Student(studentEntity.getId(), studentEntity.getFirstName(), studentEntity.getLastName());
        return student;
    }

    @Override
    public List<Student> getStudentByFirstNameStartWith(String a){
        StudentServiceImpl.startsWith = a;
        List<Student> list =  repo.getStudents();
        return list.stream().filter(student -> student.getFirstName().startsWith(StudentServiceImpl.startsWith)).collect(Collectors.toList());
    }

/*    public List<Student> getStudentByNameStartWith(String a){
        StudentServiceImpl.startsWith = a;
        List<Student> list =  repo.getStudents();
        List<Student> list1 = null;
        List<Student> list2 = null;
        list1 =  list.stream().filter(student -> student.getFirstName().startsWith(StudentServiceImpl.startsWith)).collect(Collectors.toList());
        //list2 =
    } */

 /*   @Override
    public ResponseEntity<Object> addStudent(Student student){
        return repo.addStudent(student);
    } */

    @Override
    public int addStudent(Student student){

        StudentEntity se = new StudentEntity(student.getFirstName(), student.getLastName());
        se.setId(student.getStudentId());
        StudentEntity persistedEntity = studentDtoRepository.save(se);
        return persistedEntity.getId();
    }

    @Override
    public ResponseEntity<Object> addStudentProfile(StudentProfile studentProfile){
        StudentProfileEntity studentProfileEntity = new StudentProfileEntity(studentProfile.getStudentId(), studentProfile.getSubj1(), studentProfile.getSubj2(), studentProfile.getSubj3());
        studentProfileDtoRepository.saveAndFlush(studentProfileEntity);
        return ResponseEntity.accepted().body("StudentProfile created succesfully");
    }

  /*  @Override
    public ResponseEntity<Object> getStudentProfile(int id){
        return repo.getStudentProfile(id);
    }  */

    public StudentProfile getStudentProfile(int id){
        StudentProfileEntity studentProfileEntity = studentProfileDtoRepository.findByStudentId(id);

        if(studentProfileEntity!=null) {
            return new StudentProfile(studentProfileEntity.getStudentId(), studentProfileEntity.getSubjOne(), studentProfileEntity.getSubjTwo(), studentProfileEntity.getSubjThree());
        }
        return null;
        }

    @Override
    public ResponseEntity<Object> updateStudentProfile(StudentProfile studentProfile){

        StudentProfileEntity persistedProfileEntity = studentProfileDtoRepository.findByStudentId(studentProfile.getStudentId());
        persistedProfileEntity.setSubjOne(studentProfile.getSubj1());
        persistedProfileEntity.setSubjTwo(studentProfile.getSubj2());
        persistedProfileEntity.setSubjThree(studentProfile.getSubj3());
        studentProfileDtoRepository.save(persistedProfileEntity);

        return ResponseEntity.accepted().body("Student Profile Updated Succesfully");
    }

    @Override
    public List<Student> getAllStudents(){
           return studentDtoRepository.findAll().stream().map(se -> new Student(se.getId(),se.getFirstName(),se.getLastName())).collect(Collectors.toList());
    }
    @Override
    public List<Student> Search(String param){
        return studentDtoRepository.findByFirstNameContainsOrLastNameContains(param, param)
                .stream().map(se-> new Student(se.getId(), se.getFirstName(),se.getLastName()))
                .collect(Collectors.toList());
    }
}
package com.pnc.StudentService.Repository;

import com.pnc.StudentService.Model.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface StudentDtoRepository extends JpaRepository<StudentEntity,Integer> {

    List<StudentEntity> findByFirstNameContainsOrLastNameContains(String param1, String param2);
    StudentEntity findById(int id);
}


package com.pnc.StudentService.Controller;

import com.pnc.StudentService.Model.StudentProfile;
import com.pnc.StudentService.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.pnc.StudentService.Model.Student;
import com.pnc.StudentService.Service.StudenService;

import java.util.*;

@RestController
@RequestMapping("/students/")
public class StudentServiceController {

    @Autowired
    StudentRepository studentRepo;
    @Autowired
    StudenService studentService;

    @GetMapping("")
    public List<Student> getStudents(){
           return studentService.getAllStudents();
    }

    @GetMapping("{id}")
    public ResponseEntity<Object> getStudentById(@PathVariable("id") int id){

        Student student = studentService.getStudentById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(student);
    }

    @GetMapping("firstNameStartWith/{startsWith}")
    public List<Student> getStudentByFirstNameStartWith(@PathVariable("startsWith") String str){
        return studentService.getStudentByFirstNameStartWith(str);
    }

    @PostMapping("addStudent")
    public ResponseEntity<Object> addStudent(@RequestBody Student student){
       int studentId = studentService.addStudent(student);
       return ResponseEntity.status(HttpStatus.CREATED).header("x-student-id" , Integer.toString(studentId)).build();
    }

    @PostMapping("addStudentProfile")
    public ResponseEntity<Object> addStudentProfile(@RequestBody StudentProfile studentProfile){
        return studentService.addStudentProfile(studentProfile);
    }

    @GetMapping("studentProfile/{id}")
    public ResponseEntity<Object> getStudentProfile(@PathVariable("id") int id){
         StudentProfile profile =  studentService.getStudentProfile(id);
        return ResponseEntity.accepted().body(profile);
    }

    @PutMapping("updateStudentProfile")
    public ResponseEntity<Object> updateStudentProfile(@RequestBody StudentProfile studentProfile){
        return studentService.updateStudentProfile(studentProfile);
    }

    @GetMapping("search/{param}")
    public List<Student> Search(@PathVariable("param") String param){
        return studentService.Search(param);
    }
}

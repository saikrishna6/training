package com.pnc.StudentService.Repository;

import com.pnc.StudentService.Model.StudentProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentProfileDtoRepository extends JpaRepository<StudentProfileEntity,Integer> {

    public StudentProfileEntity findByStudentId(int id);
}
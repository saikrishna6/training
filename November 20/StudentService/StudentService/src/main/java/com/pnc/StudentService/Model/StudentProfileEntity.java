package com.pnc.StudentService.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "profile")
public class StudentProfileEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int studentId;
    private int subjOne;
    private int subjTwo;
    private int subjThree;

    public StudentProfileEntity(int studentId, int subjOne, int subjTwo, int subjThree){

        this.studentId = studentId;
        this.subjOne = subjOne;
        this.subjTwo = subjTwo;
        this.subjThree = subjThree;
    }
}

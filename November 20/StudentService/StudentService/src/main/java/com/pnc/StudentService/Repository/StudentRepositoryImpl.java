package com.pnc.StudentService.Repository;

import com.pnc.StudentService.Model.StudentProfile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import com.pnc.StudentService.Model.Student;
import java.util.*;

@Repository
public class StudentRepositoryImpl implements StudentRepository {

    List<Student> students;
    Map<Integer, StudentProfile> studentToProfileMap;

   public StudentRepositoryImpl(){

       students = new ArrayList<>();
       studentToProfileMap = new HashMap<>();
       students.add(new Student(1, "Sai", "Krishna"));
       students.add(new Student(2, "Krishna ", "Telukuntla"));
       students.add(new Student(3, "Bob", "Miller"));
    }

    public List<Student> getStudents(){

       return students;
    }

    public Student getstudentById(int id){

       //students.stream().filter(student -> student.studentId == id).
        int listLen = students.size();
        Student student = null;

        for(int i = 0 ; i  < listLen; i++){
            if(students.get(i).getStudentId() ==  id){
                student = students.get(i);
                break;
            }
        }
        return student;
    }

    public ResponseEntity<Object> addStudent(final Student student){
       boolean present =  students.stream().anyMatch(s -> s.getStudentId() == student.getStudentId());
       if(present){
           return ResponseEntity.unprocessableEntity().body("Id already in use, please choose different a different Id");

       }else{
           students.add(student);
           return ResponseEntity.accepted().body("Successfully added student to the system");

       }
    }

    public ResponseEntity<Object> addStudentProfile(final StudentProfile studentProfile){

       if (studentToProfileMap.containsKey(studentProfile.getStudentId())){
           return ResponseEntity.unprocessableEntity().body("Profile already exists for the student instance, try updating the existing profile");

       }else{
           boolean presentInStudentList =  students.stream().anyMatch(s -> s.getStudentId() == studentProfile.getStudentId());
           if(presentInStudentList){
               studentToProfileMap.put(studentProfile.getStudentId(),studentProfile);
               return ResponseEntity.accepted().body("Successfully added student profile in the system");

           }else{
               return ResponseEntity.unprocessableEntity().body("Create student instance before adding student profile");
           }
       }
    }

    public ResponseEntity<Object> getStudentProfile(int id){

        if(!studentToProfileMap.containsKey(id)){
            return ResponseEntity.unprocessableEntity().body("Student Profile does not exist");

        }else{
            return ResponseEntity.accepted().body(studentToProfileMap.get(id));

        }
    }

    public ResponseEntity<Object> updateStudentProfile(StudentProfile studentProfile){
       if(studentToProfileMap.containsKey(studentProfile.getStudentId())){
           studentToProfileMap.put(studentProfile.getStudentId(), studentProfile);
           return ResponseEntity.accepted().body("Student Profile Updated Successfully");

       }else{
           return ResponseEntity.accepted().body("Student Profile Doesn't exist for Update");
       }

    }
}

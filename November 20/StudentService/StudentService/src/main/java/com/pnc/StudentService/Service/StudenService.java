package com.pnc.StudentService.Service;

import com.pnc.StudentService.Model.Student;
import com.pnc.StudentService.Model.StudentProfile;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface StudenService {

    public Student getStudentById(int id);
    public List<Student> getStudentByFirstNameStartWith(String a);
    public int addStudent(Student student);
    public ResponseEntity<Object> addStudentProfile(StudentProfile studentProfile);
    public StudentProfile getStudentProfile(int id);
    public ResponseEntity<Object> updateStudentProfile(StudentProfile studentProfile);
    public List<Student> getAllStudents();
    public List<Student> Search(String param);

    }

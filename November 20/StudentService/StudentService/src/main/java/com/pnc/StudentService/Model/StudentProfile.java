package com.pnc.StudentService.Model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentProfile {

    int studentId;
    int Subj1;
    int Subj2;
    int Subj3;

}

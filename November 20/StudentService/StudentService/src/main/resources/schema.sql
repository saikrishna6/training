Create Table student(

id INTEGER NOT NULL AUTO_INCREMENT,
first_Name varchar(255),
last_Name varchar(255),
PRIMARY KEY(id)
);

Create Table profile(

id INTEGER NOT NULL AUTO_INCREMENT,
student_id INTEGER,
Subj_one Integer,
Subj_two Integer,
Subj_three Integer,
PRIMARY KEY(id),
FOREIGN KEY(student_id) REFERENCES student(id)
);
import java.util.*;

public class MapIteration {


    // Problem-2
    public static void printMapKeyValue(Map<String, String> map) {

        map.forEach((k, v) -> System.out.println((k + ":" + v)));

    /*    Iterator<Map.Entry<String, String>> itr = map.entrySet().iterator();

        while (itr.hasNext()) {

            Map.Entry<String, String> entry = itr.next();

            //  KEY: 1 = VALUE: JOEY* KEY: 2 = VALUE: CHANDLE
            System.out.println("KEY: " + entry.getKey() + "value : " + entry.getValue());
        } */

    }
        // Problem - 3
        public static String findValue(Map<String, String> map,  String searchKey) {

            if(map.containsKey(searchKey)){

                return map.get(searchKey);

            }

            return "NOT_FOUND";

            }

         //problem - 4
         public static Map<Integer, LinkedList<String>> lengthToStringsMap(List<String> list){

            Iterator itr  = list.iterator();
            Map<Integer, LinkedList<String>> map = new HashMap<>();

           while(itr.hasNext()){

               String currStr = (String)itr.next();
               int currStrLen = currStr.length();

               if(map.containsKey(currStrLen)){

                   LinkedList<String> tempList =  map.get(currStrLen);
                   tempList.add(currStr);
                   map.put(currStrLen, tempList);

               }else{

                   LinkedList<String> tempList = new LinkedList<>();

                   tempList.add(currStr);
                   map.put(currStrLen, tempList);

               }

           }
           return map;
         }
}


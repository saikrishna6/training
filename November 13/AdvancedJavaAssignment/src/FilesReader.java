
import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Stream;

// Problem - 6
public class FilesReader {

    static List<String> listWrite;

    public static void read(String inputPath){

      try {
          Stream<Path> dirPath = Files.walk(Paths.get(String.valueOf(inputPath)));

          dirPath.filter(Files::isRegularFile).forEach(file -> {

              try {

                  Stream<String> fileData = Files.lines(file);
                  fileData.map(text -> text.toUpperCase()).forEach(convertedText -> FilesReader.listWrite.add(convertedText));
                  Files.write(Paths.get("Output.txt"), FilesReader.listWrite);

              } catch (Exception e) {

              }
          });

/*        try (BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\sktelukuntla\\file-read.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                // process the line.
                System.out.println(line);
            }
     }catch(Exception e){


        }
*/

      }catch(Exception e){

      }

    }
}

import java.util.*;

public class Scores {

    public static Map<String, Student> SubjToStudentMap;
    public static Student currStudent = null;

    public static Map<String, Student> getBestScores(List<Student> students) {

        int studentCount = students.size();

         SubjToStudentMap = new HashMap<>();

        students.stream().forEach(student -> {

            Scores.currStudent = student;
            Map<String, Integer> reportMap = student.report;
            reportMap.forEach((k, v) -> {

                if (Scores.SubjToStudentMap.containsKey(k)) {

                    if (Scores.SubjToStudentMap.get(k).report.get(k) < v) {
                        Scores.SubjToStudentMap.put(k, Scores.currStudent);

                    } else if (Scores.SubjToStudentMap.get(k).report.get(k) == v) {

                        if (Scores.SubjToStudentMap.get(k).id > Scores.currStudent.id) {

                            Scores.SubjToStudentMap.put(k, Scores.currStudent);
                        }
                    }
                } else {

                    Scores.SubjToStudentMap.put(k, Scores.currStudent);

                }

                //System.out.println((k + ":" + v));
                    }

            );

        });

    /*    Iterator itr = students.iterator();

        while(itr.hasNext()){

            Student student = (Student)itr.next();
            Map<String, Integer> reportMap = student.report;

            Iterator<Map.Entry<String, Integer>> reportMapItr = reportMap.entrySet().iterator();

            while (reportMapItr.hasNext()) {

                Map.Entry<String, Integer> entry = reportMapItr.next();
                //String currSubject =
                if( SubjToStudentMap.containsKey(entry.getKey()) ){

                    if((SubjToStudentMap.get(entry.getKey())).report.get(entry.getKey()) < entry.getValue()){

                        SubjToStudentMap.put(entry.getKey(), student);

                    }else if((SubjToStudentMap.get(entry.getKey())).report.get(entry.getKey()) == entry.getValue()){

                        if((SubjToStudentMap.get(entry.getKey()).id > student.id  )) {

                            SubjToStudentMap.put(entry.getKey() ,student);
                        }

                    }
                }else{

                    SubjToStudentMap.put(entry.getKey(), student);

                }

            }

        }
*/
  return Scores.SubjToStudentMap;


    }
}
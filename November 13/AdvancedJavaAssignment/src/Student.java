import java.util.*;

public class Student {

    int id;
    Map<String, Integer> report;

    public Student(int id, Map<String, Integer> report) {

        this.id = id;
        this.report = report;
    }

    @Override
    public String toString() {

        return "Student{" + "id='" + id + '}';
    }
}

import java.awt.datatransfer.StringSelection;
import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(50);

        // Problem-1
        System.out.println( "Problem-1 :  " +Mathematics.average(list));

        Map<String, String> map = new HashMap<String, String>();

        map.put("Key1", "Value1");
        map.put("Key2", "Value2");
        map.put("Key3", "Value3");

        // problem-2
        System.out.println("Problem-2");
        MapIteration.printMapKeyValue(map);

        //problem-3
        System.out.println("Problem-3");
       System.out.println(MapIteration.findValue(map,"Key1") );
       System.out.println(MapIteration.findValue(map,"Key11"));

       LinkedList<String> strList = new LinkedList<>();

       strList.add("Str1");
       strList.add("Str2");
       strList.add("Str22");
       strList.add("Str23");
       strList.add("Str234");

       // Problem-4
       System.out.println( "Problem-4 :  "+ MapIteration.lengthToStringsMap(strList));

       //str.replace(Student{ + id='\" +  id + }, $1) ;

        // Problem-5

      Map<String, Integer> report1 = new HashMap<>();

      // SCIENCE, MATHEMATICS, LANGUAGE, HISTORY
      report1.put("SCIENCE", 90);
      report1.put("MATHEMATICS", 85);
      report1.put("LANGUAGE",80);
      report1.put("HISTORY", 78);

       Student student1 = new Student(5, report1);

       Map<String, Integer> report2 = new HashMap<>();

        // SCIENCE, MATHEMATICS, LANGUAGE, HISTORY
        report2.put("SCIENCE", 83);
        report2.put("MATHEMATICS", 85);
        report2.put("LANGUAGE",100);
        report2.put("HISTORY", 87);

        Student student2 = new Student(8, report2);

        List<Student> students = new ArrayList<>();

        students.add(student1);
        students.add(student2);

        //problem-5 Print
        System.out.println( "Problem-5 :  " + Scores.getBestScores(students));

        // Problem - 6
        FilesReader.read("C:\\Users\\sktelukuntla\\Documents\\");

        //}
        }
}
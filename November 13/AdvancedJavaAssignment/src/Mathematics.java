import java.util.Iterator;
import java.util.List;

public class Mathematics {

    public static double average(List<Integer> list){

       return list.stream().mapToDouble(a->a).average().getAsDouble();
      /*  int listSize = list.size();
        int sum = 0;

        Iterator itr = list.iterator();

        while(itr.hasNext()){

            sum = sum + (int)itr.next();

        }
        return sum/listSize; */
    }

}

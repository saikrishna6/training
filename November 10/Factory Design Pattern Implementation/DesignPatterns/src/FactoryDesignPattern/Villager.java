package FactoryDesignPattern;

public class Villager implements Cloneable,Human, Walk {

	int dressCode = 0;
    int gender;
    
    @Override
    public Villager clone() throws CloneNotSupportedException{
		
    	Villager clonedObj = (Villager)super.clone();    	
    	return clonedObj;  	
    	
    }
    
	public void movement() {
		
		// Bussiness logic goes here.
	}
}

package FactoryDesignPattern;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class SimilarObjectTypesFactory {

	public static List<Object> generate(String type) {
		
		List<Object> list = null;
		
		Random rand = new Random(); 
	  	int count = rand.nextInt(200);
	  			
		if(type.equalsIgnoreCase("animal")) {
			
			list = new LinkedList<>();
			
			for(int i = 0 ; i < count; i++) {
				
				if(i%2 == 0) {					
					
					list.add(new Bull());
					
				}else {
					
					list.add(new Horse());					
				}				
			}		
			
		}else if(type.equalsIgnoreCase("human")) {
			
		    list = new LinkedList<>();
			
			for(int i = 0 ; i < count; i++) {
				
				if(i%2 == 0) {					
					
					list.add(new Civilian());
					
				}else {
					
					list.add(new Villager());					
				}				
			}					
			
		}		
		return list;
	}
}

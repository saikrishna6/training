package FactoryDesignPattern;

public class Bull implements Cloneable,Animal, Move{
	
	int skinTexture;
	int breed;
	
	public void movement() {
		
		// Bussiness logic goes here.
	}

	@Override
    public Bull clone() throws CloneNotSupportedException {
		
    	Bull clonedObj = (Bull)super.clone();    	
    	return clonedObj;  	
    	
    }

}

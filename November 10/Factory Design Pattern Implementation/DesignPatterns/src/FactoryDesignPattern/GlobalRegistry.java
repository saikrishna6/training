package FactoryDesignPattern;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class GlobalRegistry {
	
	public static Map<String,ArrayList<Object>> BehavToObjMap = new HashMap<>();
	
	static void populate() {
		
		ArrayList<Object> walkObjList = new ArrayList<>();
		
		walkObjList.add(new Villager());
		walkObjList.add(new Civilian());			
		
		BehavToObjMap.put("walk",walkObjList);
		
		ArrayList<Object> moveObjList = new ArrayList<>();
		
		moveObjList.add(new Villager());
		moveObjList.add(new Civilian());			
		moveObjList.add(new Horse());
		moveObjList.add(new Bull());

		BehavToObjMap.put("move", moveObjList);
		
		
	}
}

package FactoryDesignPattern;

public class Horse implements Cloneable, Animal, Move{
	
	int skinTexture;
	int breed;
	
	public void movement() {
		
		// Bussiness logic goes here.
	}
	
	@Override
    public Horse clone() throws CloneNotSupportedException{
		
    	Horse clonedObj = (Horse)super.clone();    	
    	return clonedObj;  	
    	
    }
	
}

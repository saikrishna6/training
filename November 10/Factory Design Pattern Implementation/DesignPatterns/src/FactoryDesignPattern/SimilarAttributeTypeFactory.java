package FactoryDesignPattern;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SimilarAttributeTypeFactory {

	public static List<Object> generate(String type) throws CloneNotSupportedException {
	
	LinkedList<Object> objList = new LinkedList<>();
		
	Map<String,ArrayList<Object>> BehavToObjMap = GlobalRegistry.BehavToObjMap;
	
	ArrayList<Object> prototypeObjList = BehavToObjMap.get(type);
		
	int objCount = prototypeObjList.size();
		
	Random rand = new Random(); 
  	int count = rand.nextInt(300);
  	
  	for(int i = 0; i < count ; i++) {
  		
  		  //Object c = new Civilian().clone();		 		  
  		  Object obj = prototypeObjList.get(i % ((rand.nextInt(objCount)+1)));

  		  if(obj instanceof Horse) {
  			  
     		 Horse horse = ((Horse)obj).clone();  			 
  			 objList.add(horse);
  			 
  		  }else if(obj instanceof Bull) {
  			 
  			 Bull bull = ((Bull)obj).clone(); 			  
  			 objList.add(bull);
  			
  		  }else if(obj instanceof Civilian) {
  			  
  			 Civilian civilian = ((Civilian)obj).clone(); 		  
  			 objList.add(civilian);
  			
  		  }
  		 // else if(obj instanceof Villager) {
  			  
  		//	 Villager villager = ((Villager)obj).clone(); 
  		//	 objList.add(villager);
  		 // } 
  		  
  	}
	  return objList;

	}
}

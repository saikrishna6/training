package FactoryDesignPattern;

public class Main {

	public static void main(String[] args) throws CloneNotSupportedException {
		
		System.out.println(SimilarObjectTypesFactory.generate("human") );		
		
		GlobalRegistry.populate();		
		System.out.println(SimilarAttributeTypeFactory.generate("walk"));
	}
	
}

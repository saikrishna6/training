package FactoryDesignPattern;

public interface Human {

	int eyes = 2;
	int limbs = 4;
	 
	void movement();
}

package FactoryDesignPattern;

public class Civilian implements Cloneable,Human, Walk {

    int dressCode = 1; 
    int gender;
    
	public void movement() {
		
		// Bussiness logic goes here.
	}
	
	@Override
    public Civilian clone() throws CloneNotSupportedException {
		
    	Civilian clonedObj = (Civilian)super.clone();    	
    	return clonedObj;  	
    	
    }
}

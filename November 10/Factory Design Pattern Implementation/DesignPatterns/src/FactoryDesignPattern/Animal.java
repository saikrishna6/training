package FactoryDesignPattern;

public interface Animal {
   
	int eyes = 2;
	void movement();
	
}
